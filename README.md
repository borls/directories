# Directories Application
Тестовый пример, который позволяет ходить по дирректориям и просматривать информацию

Макет находится в папке ```img/layout```

### Installation

Прежде всего следует создать клон репозитория и перейти в папку с проектом:

```sh
$ git clone https://borls@bitbucket.org/borls/directories.git; cd directories;
```

Затем установить npm и bower:

```sh
$ npm install; bower install
```

Запуск проекта на локальной машине
```sh
$ npm start
```

Открыть сайт в браузере
```sh
http://localhost:8000/
```
