'use strict';

/* Controllers */
directoryApp.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider
        .when('/', {
            templateUrl: 'template/directories.html',
            controller: 'DirectoryListCtrl'
        })
        .when('/directory/:directoryId', {
            templateUrl: 'template/directory.html',
            controller: 'DirectoryCtrl'
        })
        .when('/directory/:directoryId/field/:fieldId', {
            templateUrl: 'template/field.html',
            controller: 'FieldCtrl'
        })
        .otherwise({
            redirectTo: '/'
        })
}]);

directoryApp.controller(
    'DirectoryListCtrl',
    ['$scope', '$http', '$location',
        function ($scope, $http, $location) {
            $http.get('json/getDirectories.json')
                .success(function (data, status, headers, config) {
                    $scope.directories = data.response.directories;
                    $scope.title = "Справочники";
                })
                .error(function () {
                });


        }
    ]
);

directoryApp.controller(
    'DirectoryCtrl',
    ['$scope', '$http', '$location', '$routeParams',
        function ($scope, $http, $location, $routeParams) {
            $scope.directoryId = $routeParams.directoryId;

            $http.get('json/getDirectory.json')
                .success(function (data, status, headers, config) {
                    $scope.directory = data.response.directory;
                    $scope.fields = $scope.directory.fields;
                    $scope.items = $scope.directory.items;
                    $scope.title = $scope.directory.name;
                    $scope.location = [{path: '/', name: 'Справочники'}];
                })
                .error(function () {
                });


        }
    ]
);

directoryApp.controller(
    'FieldCtrl',
    ['$scope', '$http', '$location', '$routeParams',
        function ($scope, $http, $location, $routeParams) {
            $scope.fieldId = $routeParams.fieldId;

            $http.get('json/getEntry.json')
                .success(function (data, status, headers, config) {
                    $scope.field = data.response.entry;
                    $scope.directory = $scope.field.directory;
                    $scope.items = $scope.field.items;
                    $scope.title = $scope.field.name;

                    for(var i=0; i<$scope.items.length; i++) {
                        if ($scope.items[i].type == 'DATE') {
                            $scope.items[i].value = new Date($scope.items[i].value)
                        }
                    }

                    $scope.location = [
                        {
                            path: '/',
                            name: 'Справочники'
                        },
                        {
                            path:'/directory/' + $scope.directory.id,
                            name: $scope.directory.name
                        }
                    ];
                })
                .error(function () {
                });
        }
    ]
);