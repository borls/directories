'use strict';

/* Filters */
directoryApp.filter('dotDate', function () {
    return function (input) {
        return (new Date(input)).getTime();
    }
});