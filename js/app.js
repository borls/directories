'use strict';

/* App Module */
var directoryApp = angular.module('directoryApp', ['ngRoute']);

directoryApp.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider
        .when('/', {
            templateUrl: 'template/directories.html',
            controller: 'DirectoryListCtrl'
        })
        .when('/directory/:directoryId', {
            templateUrl: 'template/directory.html',
            controller: 'DirectoryCtrl'
        })
        .when('/directory/:directoryId/field/:fieldId', {
            templateUrl: 'template/field.html',
            controller: 'FieldCtrl'
        })
        .otherwise({
            redirectTo: '/'
        })
}]);